<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $area_schools_id
 * @property int $school_languages_id
 * @property int $holding_areas_id
 * @property int $goverments_id
 * @property int $school_type
 * @property string $name
 * @property string $created_at
 * @property string $updated_at
 * @property AreaSchool $areaSchool
 * @property Goverment $goverment
 * @property HoldingArea $holdingArea
 * @property SchoolLanguage $schoolLanguage
 * @property TypeSchool $typeSchool
 * @property SchoolsBranch[] $schoolsBranches
 */
class Schools extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['data', 'notes','longitude', 'latitudes','school_languages_id', 'holding_areas_id', 'goverments_id', 'school_type', 'name', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getLocationAttribute()
    {
        return (object)[
            'latitude' => $this->latitudes,
            'longitude' => $this->longitude,
        ];
    }


    /*
    Transform the returned value from the Nova field
    */
    public function setLocationAttribute($value)
    {
        $location_lat = round(object_get($value, 'latitude'), 7);
        $location_lng = round(object_get($value, 'longitude'), 7);
        $this->attributes['latitudes'] = $location_lat;
        $this->attributes['longitude'] = $location_lng;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function goverment()
    {
        return $this->belongsTo('App\Goverment', 'goverments_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function holdingArea()
    {
        return $this->belongsTo('App\HoldingArea', 'holding_areas_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function schoolLanguage()
    {
        return $this->belongsTo('App\SchoolLanguages', 'school_languages_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function typeSchool()
    {
        return $this->belongsTo('App\TypeSchool', 'school_type');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function schoolsBranches()
    {
        return $this->hasOne('App\SchoolsBranches','school_id');
    }
}
