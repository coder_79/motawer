<?php

namespace App\Nova;

use Acm\NovaGmap\NovaGmap;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\HasOne;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Markdown;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;
use Whitecube\NovaFlexibleContent\Flexible;

class Schools extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Schools';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'name',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),
            Text::make('Name','name')->rules('required')->sortable(),
            Flexible::make('Data','data')
                ->addLayout('Content', 'wysiwyg', [
                    Text::make('Address','address')->rules('required'),
                    Number::make('Phone','phone'),
                    Number::make('Mobile','mobile'),
                    Text::make('Email','email'),
                    Text::make('Facebook','fb'),
                ]),
            Flexible::make('Notes','notes')
                ->addLayout('Content', 'wysiwyg', [
                    Text::make('Title'),
                    Markdown::make('Content'),
                ]),
            NovaGmap::make('Location'),
            BelongsTo::make('Holding Area','holdingArea','App\Nova\HeadArea')->sortable(),
            BelongsTo::make('School Languages','schoolLanguage','App\Nova\SchoolLanguage')->sortable(),
            BelongsTo::make('Type School','typeSchool','App\Nova\TypeSchool')->sortable(),
            BelongsTo::make('Government','goverment','App\Nova\Goverment')->sortable(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
