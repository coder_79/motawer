<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AreaSchool extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['name', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function schools()
    {
        return $this->hasMany('App\Schools', 'area_schools_id');
    }
}
