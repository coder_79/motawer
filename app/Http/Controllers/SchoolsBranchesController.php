<?php

namespace App\Http\Controllers;

use App\SchoolsBranches;
use Illuminate\Http\Request;

class SchoolsBranchesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SchoolsBranches  $schoolsBranches
     * @return \Illuminate\Http\Response
     */
    public function show(SchoolsBranches $schoolsBranches)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SchoolsBranches  $schoolsBranches
     * @return \Illuminate\Http\Response
     */
    public function edit(SchoolsBranches $schoolsBranches)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SchoolsBranches  $schoolsBranches
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SchoolsBranches $schoolsBranches)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SchoolsBranches  $schoolsBranches
     * @return \Illuminate\Http\Response
     */
    public function destroy(SchoolsBranches $schoolsBranches)
    {
        //
    }
}
