<?php

namespace App\Http\Controllers;

use App\GovermentControl;
use Illuminate\Http\Request;

class GovermentControlController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\GovermentControl  $govermentControl
     * @return \Illuminate\Http\Response
     */
    public function show(GovermentControl $govermentControl)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\GovermentControl  $govermentControl
     * @return \Illuminate\Http\Response
     */
    public function edit(GovermentControl $govermentControl)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\GovermentControl  $govermentControl
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GovermentControl $govermentControl)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\GovermentControl  $govermentControl
     * @return \Illuminate\Http\Response
     */
    public function destroy(GovermentControl $govermentControl)
    {
        //
    }
}
