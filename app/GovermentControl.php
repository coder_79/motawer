<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GovermentControl extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['name', 'created_at', 'updated_at'];

}
