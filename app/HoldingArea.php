<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $created_at
 * @property string $updated_at
 * @property School[] $schools
 */
class HoldingArea extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['name','area_id', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function schools()
    {
        return $this->hasMany('App\Schools', 'holding_areas_id');
    }
    public function areaSchool()
    {
        return $this->belongsTo('App\AreaSchool', 'area_id');
    }

}
